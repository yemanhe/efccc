package com.example.efccc;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.view.View;

public class MenuLandSector extends AppCompatActivity {


    CardView landCover, projData;
    String parentKey;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_land_sector);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        Intent intent = getIntent();
        parentKey = intent.getStringExtra("pageKey");

        landCover = findViewById(R.id.cardView_landcover);
        projData = findViewById(R.id.cardView_projdata);

        landCover.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MenuLandSector.this, LandForm.class);
                intent.putExtra("page","Land Cover Type Form");
                intent.putExtra("pageKey",parentKey+1);
                startActivity(intent);
            }
        });


        projData.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MenuLandSector.this, MenuProjectData.class);
                intent.putExtra("pageKey",parentKey+2);
                startActivity(intent);
            }
        });

    }

}
