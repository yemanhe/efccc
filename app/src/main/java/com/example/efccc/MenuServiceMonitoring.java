package com.example.efccc;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.view.View;

public class MenuServiceMonitoring extends AppCompatActivity {


    CardView smInd, smPerf;
    String parentKey;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_service_monitoring);


        Toolbar toolbar = findViewById(R.id.servicem_toolbar);
        setSupportActionBar(toolbar);
        toolbar.setTitle("Service Monitoring");

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        Intent intent = getIntent();
        parentKey = intent.getStringExtra("pageKey");

        smInd = findViewById(R.id.cardView_servicem_indicator);
        smPerf = findViewById(R.id.cardView_servicem_performance);

        smInd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MenuServiceMonitoring.this, Tabbed.class);
                intent.putExtra("page", "Service Monitoring Indicator");
                intent.putExtra("pageKey", parentKey+1);
                startActivity(intent);
            }
        });


        smPerf.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MenuServiceMonitoring.this, Tabbed.class);
                intent.putExtra("page", "Service Monitoring Performance");
                intent.putExtra("pageKey", parentKey+2);
                startActivity(intent);
            }
        });
    }
}
