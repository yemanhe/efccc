package com.example.efccc;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.view.View;

public class MenuProjectData extends AppCompatActivity {

    CardView pInd, pPerf;
    String parentKey;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_project_data);


        Toolbar toolbar = findViewById(R.id.projectData_toolbar);
        setSupportActionBar(toolbar);
        toolbar.setTitle("Project Data");

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        Intent intent = getIntent();
        parentKey = intent.getStringExtra("pageKey");

        pInd = findViewById(R.id.cardView_indicator);
        pPerf = findViewById(R.id.cardView_performance);

        pInd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MenuProjectData.this, Tabbed.class);
                intent.putExtra("page", "Project Data Indicator");
                intent.putExtra("pageKey", parentKey+1);
                startActivity(intent);
            }
        });


        pPerf.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MenuProjectData.this, Tabbed.class);
                intent.putExtra("page", "Project Data Performance");
                intent.putExtra("pageKey", parentKey+2);
                startActivity(intent);
            }
        });
    }
}
