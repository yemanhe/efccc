package com.example.efccc;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

public class ApiClient {

//    public static final String BASE_URL = "http://yemanem-001-site2.itempurl.com/api/";
    public static final String BASE_URL = "http://196.188.95.53:8081/api/";
    public static final String USSDNUM = "6572";
    private static Retrofit retrofit = null;


    public static Retrofit getClient() {
        if (retrofit==null) {

//            Gson gson = new GsonBuilder()
//                    .setLenient()
//                    .create();

            retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(ScalarsConverterFactory.create())
                    .addConverterFactory(GsonConverterFactory.create())


                    .build();

        }
        return retrofit;
    }
}


/**
 * Todo: Task Reminder
 *  1) permission on start => https://medium.com/programming-lite/runtime-permissions-in-android-7496a5f3de55
 *  2) location fetcher improvement
 *  3) send data through internet -> prepared and ready waiting Y
 *  4) know if there is an internet connection and connect if not get data from share pref
 */
