package com.example.efccc;


import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.app.AppCompatDialogFragment;
import android.telephony.SmsManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.example.efccc.ApiClient.USSDNUM;
import static com.example.efccc.LandForm.hideKeyboardForm;


public class OneFragment extends Fragment {
    ProgressDialog dialog;
    Spinner f1Indi;
    EditText f1Value,f1Date;
    String msg;
    String pageKey;
    String newPageKey;
    Spinner menu1;
    String f1IndiH,f1ValueH,f1DateH;
    private OnFragmentInteractionListener mListener;
    String[] projectStage = {
            "Select Level",
            "Add New BaseLine",
            "Add New Plan",
            "Add New Accomplishment"
    };

    String selectedDate;

    SharedPreferences myPref;
    SharedPreferences.Editor myeditor;

    public static final int REQUEST_CODE = 11;

    public OneFragment() {

    }

    public static OneFragment newInstance() {
        OneFragment fragment = new OneFragment();
        return fragment;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

//        myPref = getContext().getSharedPreferences("pref", 0);

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {


        View view = inflater.inflate(R.layout.fragment_one, container, false);
        myPref = getContext().getSharedPreferences("pref", 0);


        pageKey = getArguments().getString("pageKey");
        pageKey = pageKey+1;

        dialog = new ProgressDialog(getContext());
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(true);

        final FragmentManager fm = ((AppCompatActivity)getActivity()).getSupportFragmentManager();


        f1Indi = view.findViewById(R.id.f1_form_indicator_txt);
        f1Value = view.findViewById(R.id.f1_form_value_txt);
        f1Date = view.findViewById(R.id.f1_form_date_txt);
        menu1 = view.findViewById(R.id.f1_Spinner_projectStage);

        loadIndicatorManagement();

        Button btnsend = view.findViewById(R.id.f1_btn_send);


        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_dropdown_item_1line, projectStage) {
            @Override
            public boolean isEnabled(int position){

                return position != 0;
            }
            @Override
            public View getDropDownView(int position, View convertView,
                                        @NonNull ViewGroup parent) {
                View view = super.getDropDownView(position, convertView, parent);
                TextView tv = (TextView) view;
                if(position == 0){
                    tv.setTextColor(getResources().getColor(R.color.grey_400));
                }
                else {
                    tv.setTextColor(getResources().getColor(R.color.green_800));
                }
                tv.setBackgroundColor(getResources().getColor(R.color.green_50));
                return view;
            }
        };
        adapter.setDropDownViewResource(R.layout.spinner_dropdown_item);
        menu1.setAdapter(adapter);
        menu1.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                newPageKey = pageKey+position;
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });




        f1Date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // create the datePickerFragment
                AppCompatDialogFragment newFragment = new DatePickerFragment();
                // set the targetFragment to receive the results, specifying the request code
                newFragment.setTargetFragment(OneFragment.this, REQUEST_CODE);
                // show the datePicker
                newFragment.show(fm, "datePicker");
            }
        });




        btnsend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                validation();
                hideKeyboardForm(getContext(),v);
            }
        });
        return view;

    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        // check for the results
        if (requestCode == REQUEST_CODE && resultCode == Activity.RESULT_OK) {
            // get date from string
            selectedDate = data.getStringExtra("selectedDate");
            // set the value of the editText
            f1Date.setText(selectedDate);
        }
    }
    public void validation(){
        f1IndiH = f1Indi.getSelectedItem() != null ? f1Indi.getSelectedItem().toString()  : null ;
        f1ValueH =f1Value.getText().toString();
        f1DateH = f1Date.getText().toString();
        f1Date.setText(selectedDate);
        if((f1IndiH != null) && (!f1IndiH.isEmpty()) && (!f1IndiH.equals("Select Indicator Management")) &&
                (f1ValueH != null) && (!f1ValueH.isEmpty()) && (!f1ValueH.equals("null")) &&
                (f1DateH != null) && (!f1DateH.isEmpty()) && (!f1DateH.equals("null")) &&
                (newPageKey != null) && (!newPageKey.isEmpty()) && (!newPageKey.equals("null")) && (!newPageKey.endsWith("0"))

        ){
            msg = newPageKey + " ! " + menu1.getSelectedItem().toString()+" ! "+ f1IndiH + " ! " + f1ValueH+ " ! " + f1DateH;
            sendMessage(msg);


        }else {

            Toast.makeText(getContext(), "Please Complete the From", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    public void sendMessage(String msg) {

        final String zmsg = msg;

        if (ContextCompat.checkSelfPermission(getContext(), Manifest.permission.SEND_SMS) != PackageManager.PERMISSION_GRANTED &&
                ContextCompat.checkSelfPermission(getContext(), Manifest.permission.INTERNET) != PackageManager.PERMISSION_GRANTED
        ) {
            ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.SEND_SMS}, 1);
            ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.INTERNET}, 2);
        } else {


            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
            Call<String> call = apiService.sendData(msg,myPref.getString("PhoneNum", null));
            call.enqueue(new Callback<String>() {

                @Override
                public void onResponse(Call<String> call, Response<String> response) {
                    if(response.isSuccessful()){

                        Toast.makeText(getContext(),"Message sent through internet",Toast.LENGTH_LONG).show();
                        f1Indi.setSelection(0,true);
                        f1Value.setText("");
                        f1Date.setText("");
                        f1IndiH = "";
                        f1ValueH = "";
                        f1DateH = "";
                        menu1.setSelection(0);
                    }
                }

                @Override
                public void onFailure(Call<String> call, Throwable t) {
                    Toast.makeText(getContext(),"Network Error",Toast.LENGTH_LONG).show();


                    try{
                        SmsManager smsMgr = SmsManager.getDefault();
                        smsMgr.sendTextMessage(USSDNUM,null, zmsg,null,null);
                        Toast.makeText(getContext(),"Message Sent !", Toast.LENGTH_LONG).show();
                        f1Indi.setSelection(0,true);
                        f1Value.setText("");
                        f1Date.setText("");
                        f1IndiH = "";
                        f1ValueH = "";
                        f1DateH = "";
                        menu1.setSelection(0);
                    }catch (Exception e){
                        Toast.makeText(getContext(),"Unable To Send Message, Please Try again",Toast.LENGTH_SHORT).show();
                    }


                }
            });


        }
    }





    public void loadIndicatorManagement() {


        dialog.show();
        myeditor = myPref.edit();
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<List<Item>> call = apiService.getOutPuts();
        call.enqueue(new Callback<List<Item>>() {

            @Override
            public void onResponse(Call<List<Item>> call, Response<List<Item>> response) {
                int statusCode = response.code();

                List<Item> inidManagementres = response.body();

                if (statusCode == 200) {

                    String[] myList = new String[inidManagementres.size()+1];
                    myList[0] = "Select Indicator Management";
                    int index = 1;
                    for (Item itm : inidManagementres) {
                        myList[index] =  itm.itemName;
                        index++;
                    }

                    renderSpinner(myList);

                    Gson gson = new Gson();
                    String res = gson.toJson(inidManagementres);

                    myeditor.putString("indicatorManagement", res);
                    myeditor.apply();

                    Toast.makeText(getContext(), "Updating", Toast.LENGTH_LONG).show();

                }

            }

            @Override
            public void onFailure(Call<List<Item>> call, Throwable t) {
                // Log error here since request failed
                dialog.dismiss();

                List<Item> arrayItems;

                String indiM = myPref.getString("indicatorManagement", null);

                if (indiM != null) {
                    Gson gson = new Gson();
                    Type type = new TypeToken<List<Item>>() {
                    }.getType();
                    arrayItems = gson.fromJson(indiM, type);


                    String[] myList = new String[arrayItems.size() + 1];
                    myList[0] = "Select Indicator Management";
                    int index = 1;
                    for (Item itm : arrayItems) {
                        myList[index] = itm.itemName;
                        index++;
                    }

                    renderSpinner(myList);
                }else{
                    Toast.makeText(getContext(), "List of Indicator Management is Empty",Toast.LENGTH_SHORT).show();

                }

            }
        });

        dialog.dismiss();



    }



    public void renderSpinner(String[] myList){

            ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_dropdown_item_1line, myList) {
                @Override
                public boolean isEnabled(int position) {

                    return position != 0;
                }

                @Override
                public View getDropDownView(int position, View convertView,
                                            @NonNull ViewGroup parent) {
                    View view = super.getDropDownView(position, convertView, parent);
                    TextView tv = (TextView) view;
                    if (position == 0) {
                        tv.setTextColor(getResources().getColor(R.color.grey_400));
                    } else {
                        tv.setTextColor(getResources().getColor(R.color.green_800));
                    }
                    tv.setBackgroundColor(getResources().getColor(R.color.green_50));
                    return view;
                }
            };
            adapter.setDropDownViewResource(R.layout.spinner_dropdown_item);
            f1Indi.setAdapter(adapter);
            f1Indi.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });

        }


}



