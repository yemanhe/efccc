package com.example.efccc;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.view.View;

public class MenuEcosystemSector extends AppCompatActivity {

    CardView ecosystem, serviceM;
    String parentKey;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ecosystem_sector);

        Toolbar toolbar = findViewById(R.id.ecosystem_sector_toolbar);
        setSupportActionBar(toolbar);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        Intent intent = getIntent();
        parentKey = intent.getStringExtra("pageKey");

        ecosystem = findViewById(R.id.cardView_ecosystem);
        serviceM = findViewById(R.id.cardView_serviceM);

        ecosystem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MenuEcosystemSector.this, LandForm.class);
                intent.putExtra("page","Ecosystem Data Form");
                intent.putExtra("pageKey",parentKey+1);
                startActivity(intent);
            }
        });


        serviceM.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MenuEcosystemSector.this, MenuServiceMonitoring.class);
                intent.putExtra("pageKey",parentKey+2);
                startActivity(intent);
            }
        });
    }
}
