package com.example.efccc;

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.telephony.SmsManager;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.example.efccc.ApiClient.USSDNUM;

public class LandForm extends AppCompatActivity implements LocationListener {

    Button btn_send;
    EditText Area;
    Spinner landType, kebeleName;
    String intentPage;
    String Latitude, Longitude;
    String pageKey;
    ProgressDialog dialog;
    private static final int REQUEST_LOCATION = 1;
    public String phone;
    LocationManager locationManager;
    public Context myContext;
    public View myView;
    public Boolean btnClick = false;
    boolean GPS = false;
    boolean NTK = false;
    public String[] landEcoList = {"Select"};
    public String[] kebeleList = {"Select"};
//
    SharedPreferences myPref;
    SharedPreferences.Editor myeditor;


    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.land_form);

        myPref = getApplicationContext().getSharedPreferences("pref", 0);

        phone = myPref.getString("PhoneNum", "notfound");
        boolean auth = myPref.getBoolean("Is_Auth", false);

        if(!auth){
            finish();
        }

        Toolbar toolbar = findViewById(R.id.new_form_toolbar);
        setSupportActionBar(toolbar);

        Intent intent = getIntent();
        intentPage = intent.getStringExtra("page");
        pageKey = intent.getStringExtra("pageKey");
        myContext = getApplicationContext();
        myView = getWindow().getDecorView();


        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setTitle(intentPage);

        }
        dialog = new ProgressDialog(LandForm.this);
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(true);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });


        btn_send = findViewById(R.id.btn_send);
        kebeleName = findViewById(R.id.editText_1);
        landType = findViewById(R.id.Spinner_landcovertype);
        Area = findViewById(R.id.editText_3);


        statusCheck();

        LoadKebele();

        LoadLandType();



     btn_send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if (kebeleName.getSelectedItem() != null &&
                        landType.getSelectedItem() != null &&
                        (!kebeleName.getSelectedItem().toString().equals("Select Kebele")) &&
                        (!kebeleName.getSelectedItem().toString().equals("Select")) &&
                        (!Area.getText().toString().isEmpty()) && (!Area.getText().toString().equals("null")) &&
                        (!landType.getSelectedItem().toString().equals("Select Land Type")) &&
                        (!landType.getSelectedItem().toString().equals("Select Service Type")) &&
                        (!landType.getSelectedItem().toString().equals("Select"))

                ) {

                    btnClick = true;
//
                    if (btnClick && kebeleName != null && landType != null) {

                        String msg = pageKey + " ! " + kebeleName.getSelectedItem().toString() + " ! " + landType.getSelectedItem().toString() + " ! " + Area.getText() + " ! " + Longitude + " ! " + Latitude;
                        sendMessage(msg);
                        hideKeyboardForm(myContext, myView);
                    }

                } else {
                    btnClick = false;
                    Toast.makeText(getApplicationContext(), "Please Complete the From", Toast.LENGTH_LONG).show();
                }


            }
        });
    }


    public void sendMessage(String msg) {


        dialog.setMessage("Sending..");
        dialog.show();
        final String zmsg = msg;

        if (ContextCompat.checkSelfPermission(LandForm.this, Manifest.permission.SEND_SMS) != PackageManager.PERMISSION_GRANTED &&
                ContextCompat.checkSelfPermission(LandForm.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                ContextCompat.checkSelfPermission(LandForm.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(LandForm.this, new String[]{Manifest.permission.SEND_SMS}, 1);
            ActivityCompat.requestPermissions(LandForm.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);
            ActivityCompat.requestPermissions(LandForm.this, new String[]{Manifest.permission.ACCESS_COARSE_LOCATION}, 1);


        } else {



            if (btnClick && (Longitude != null) && (Latitude != null) &&
                    !Longitude.equals(null) && !Latitude.equals(null) &&
                    (!Longitude.isEmpty()) && (!Longitude.equals("null")) &&
                     (!Latitude.isEmpty()) && (!Latitude.equals("null"))) {


                ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
                Call<String> call = apiService.sendData(msg, myPref.getString("PhoneNum", "notfound"));
                call.enqueue(new Callback<String>() {

                    @Override
                    public void onResponse(Call<String> call, Response<String> response) {
                        if (response.isSuccessful()) {

                            Toast.makeText(getApplicationContext(), "Message sent through internet", Toast.LENGTH_LONG).show();
                            Toast.makeText(LandForm.this, zmsg, Toast.LENGTH_LONG).show();

                            kebeleName.setSelection(0);
                            landType.setSelection(0);
                            Area.setText("");
                            Longitude = null;
                            Latitude = null;
                            btnClick = false;
                        }

                    }

                    @Override
                    public void onFailure(Call<String> call, Throwable t) {

                        try {
                            SmsManager smsMgr = SmsManager.getDefault();
                            smsMgr.sendTextMessage(USSDNUM, null, zmsg, null, null);

                            Toast.makeText(getApplicationContext(), "Message sent through sms", Toast.LENGTH_LONG).show();

                            Toast.makeText(LandForm.this, zmsg, Toast.LENGTH_LONG).show();

                            kebeleName.setSelection(0);
                            landType.setSelection(0);
                            Area.setText("");
                            Longitude = null;
                            Latitude = null;
                            btnClick = false;

                        } catch (Exception e) {
                            Toast.makeText(LandForm.this, "Unable To Send Message, Please Try again", Toast.LENGTH_SHORT).show();
                            btnClick = false;

                        }

                    }
                });
            }else{
                try {
                    SmsManager smsMgr = SmsManager.getDefault();
                    smsMgr.sendTextMessage(USSDNUM, null, zmsg, null, null);

                    Toast.makeText(getApplicationContext(), "Message sent through sms", Toast.LENGTH_LONG).show();

                    Toast.makeText(LandForm.this, zmsg, Toast.LENGTH_LONG).show();

                    kebeleName.setSelection(0);
                    landType.setSelection(0);
                    Area.setText("");
                    Longitude = null;
                    Latitude = null;
                    btnClick = false;

                } catch (Exception e) {
                    Toast.makeText(LandForm.this, "Unable To Send Message, Please Try again", Toast.LENGTH_SHORT).show();
                    btnClick = false;

                }
            }

        }

        dialog.dismiss();

    }


    public void statusCheck() {


        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        GPS = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        NTK = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);

        if (!GPS && !NTK) {
            buildAlertMessageNoGps();
        } else {

            if (NTK) {
                try {

                    locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
                    locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, this);

                } catch (SecurityException e) {
                    e.printStackTrace();
                }
            }

            if (GPS) {
                try {

                    locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
                    locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, this);

                } catch (SecurityException e) {
                    e.printStackTrace();
                }
            }

        }

        hideKeyboardForm(myContext, myView);
        if( !GPS && !NTK ){
            Toast.makeText(myContext, "Unable to get your location", Toast.LENGTH_SHORT).show();
            Toast.makeText(myContext, "Please, Make sure you are connected to internet", Toast.LENGTH_SHORT).show();
            Toast.makeText(myContext, "and allow permission for location", Toast.LENGTH_SHORT).show();

        }

    }

    @Override
    public void onLocationChanged(Location location) {
        Longitude = location.getLatitude() + "";
        Latitude = location.getLongitude() + "";



//        if (btnClick && kebeleName != null && landType != null) {
//
//                String msg = pageKey + " ! " + kebeleName.getSelectedItem().toString() + " ! " + landType.getSelectedItem().toString() + " ! " + Area.getText() + " ! " + Longitude + " ! " + Latitude;
//                sendMessage(msg);
//                hideKeyboardForm(myContext, myView);
//
//        }

//            dialog.dismiss();



//        if (btnClick && !Longitude.equals(null) && !Latitude.equals(null)) {
//
//            if ((Longitude != null) && (!Longitude.isEmpty()) && (!Longitude.equals("null")) &&
//                    (Latitude != null) && (!Latitude.isEmpty()) && (!Latitude.equals("null"))) {
//
//                String msg = pageKey + " ! " + kebeleName.getSelectedItem().toString() + " ! " + landType.getSelectedItem().toString() + " ! " + Area.getText() + " ! " + Longitude + " ! " + Latitude;
//                sendMessage(msg);
//                hideKeyboardForm(myContext, myView);
//
//            } else {
//                dialog.setMessage("Getting Your Location...");
//                dialog.show();
//            }
//
//            dialog.dismiss();
//
//        }

    }


    @Override
    public void onProviderDisabled(String provider) {
        Toast.makeText(LandForm.this, "Please Enable GPS and Internet", Toast.LENGTH_SHORT).show();
    }


    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }


    @Override
    public void onProviderEnabled(String provider) {
        Toast.makeText(getApplicationContext(), "GPS Enabled", Toast.LENGTH_SHORT).show();

    }


    private void buildAlertMessageNoGps() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Your GPS seems to be disabled, do you want to enable it?")
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, final int id) {
                        startActivity(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, final int id) {
                        dialog.cancel();
                    }
                });
        final AlertDialog alert = builder.create();
        alert.show();
    }



    public static void hideKeyboardForm(Context context, View myView) {
        InputMethodManager imm = (InputMethodManager) context.getSystemService(Activity.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(myView.getWindowToken(), 0);
    }


    public void LoadKebele() {

        if (ContextCompat.checkSelfPermission(LandForm.this, Manifest.permission.INTERNET) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(LandForm.this, new String[]{Manifest.permission.INTERNET}, 1);
        } else {
            dialog.show();

            myeditor = myPref.edit();

            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
            Call<List<Item>> call = apiService.getKebele(phone);
            call.enqueue(new Callback<List<Item>>() {

                @Override
                public void onResponse(Call<List<Item>> call, Response<List<Item>> response) {
                    int statusCode = response.code();
                    List<Item> serverResponse = response.body();
                    if (statusCode == 200) {
                        kebeleList = new String[serverResponse.size() + 1];

                        kebeleList[0] = "Select Kebele";
                        int index = 1;
                        for (Item itm : serverResponse) {
                            kebeleList[index] = itm.itemName;
                            index++;
                        }

                        renderKebeleSpiner(kebeleList);

                        Gson gson = new Gson();
                        String res = gson.toJson(serverResponse);

                        myeditor.putString("kebele", res);
                        myeditor.commit();

                        Toast.makeText(getApplicationContext(), "Updating Kebele", Toast.LENGTH_SHORT).show();


                    }
                    dialog.dismiss();
                }


                @Override
                public void onFailure(Call<List<Item>> call, Throwable t) {
                    // Log error here since request failed
                    dialog.dismiss();

                    List<Item> arrayItems;
                    String storeKebele = myPref.getString("kebele", null);
                    if (storeKebele != null) {
                        Gson gson = new Gson();
                        Type type = new TypeToken<List<Item>>() {
                        }.getType();
                        arrayItems = gson.fromJson(storeKebele, type);


                        kebeleList = new String[arrayItems.size() + 1];


                        kebeleList[0] = "Select Kebele";
                        int index = 1;
                        for (Item itm : arrayItems) {
                            kebeleList[index] = itm.itemName;
                            index++;
                        }

                        renderKebeleSpiner(kebeleList);
                    }else {
                    Toast.makeText(getApplicationContext(), "Kebele list is empty", Toast.LENGTH_SHORT).show();
                    }
                }
            });

        }
    }


    public void renderKebeleSpiner(String[] mykebeleList){
            ArrayAdapter<String> adapter = new ArrayAdapter<String>(LandForm.this, android.R.layout.simple_dropdown_item_1line, mykebeleList) {
                @Override
                public boolean isEnabled(int position) {

                    return position != 0;
                }

                @Override
                public View getDropDownView(int position, View convertView,
                                            @NonNull ViewGroup parent) {
                    View view = super.getDropDownView(position, convertView, parent);
                    TextView tv = (TextView) view;
                    if (position == 0) {
                        tv.setTextColor(getResources().getColor(R.color.grey_400));
                    } else {
                        tv.setTextColor(getResources().getColor(R.color.green_800));
                    }
                    tv.setBackgroundColor(getResources().getColor(R.color.green_50));
                    return view;
                }
            };
            adapter.setDropDownViewResource(R.layout.spinner_dropdown_item);
            kebeleName.setAdapter(adapter);
            kebeleName.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });


        }


    public void LoadLandType() {
        if (ContextCompat.checkSelfPermission(LandForm.this, Manifest.permission.INTERNET) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(LandForm.this, new String[]{Manifest.permission.INTERNET}, 1);
        } else {
            dialog.show();
            myeditor = myPref.edit();


            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

            Call<List<Item>> call = pageKey.equals("11") ? apiService.getLandCoverType() : apiService.getEcoSystemType();

            call.enqueue(new Callback<List<Item>>() {

                @Override
                public void onResponse(Call<List<Item>> call, Response<List<Item>> response) {
                    int statusCode = response.code();
                    List<Item> ServrResp = response.body();

                    if (statusCode == 200) {


                        landEcoList = new String[ServrResp.size() + 1];
                        landEcoList[0] = pageKey.equals("11") ? "Select Land Type" : "Select Service Type";

                        int index = 1;
                        for (Item itm : ServrResp) {
                            landEcoList[index] = itm.itemName;
                            index++;
                        }


                        renderLandEcoSpinner(landEcoList);


                        Gson gson = new Gson();
                        String res = gson.toJson(ServrResp);

                        if (pageKey.equals("11")) {
                            myeditor.putString("landType", res);
                        } else {
                            myeditor.putString("serviceType", res);
                        }

                        myeditor.commit();

                        Toast.makeText(getApplicationContext(), "Updating", Toast.LENGTH_LONG).show();


                    }
                    dialog.dismiss();
                }

                @Override
                public void onFailure(Call<List<Item>> call, Throwable t) {

                    List<Item> arrayItems;

                    String LandandServiceType = pageKey.equals("11") ? myPref.getString("landType", null) : myPref.getString("serviceType", null);

                    if (LandandServiceType != null) {
                        Gson gson = new Gson();
                        Type type = new TypeToken<List<Item>>() {
                        }.getType();
                        arrayItems = gson.fromJson(LandandServiceType, type);


                        landEcoList = new String[arrayItems.size() + 1];
                        landEcoList[0] = pageKey.equals("11") ? "Select Land Type" : "Select Service Type";

                        int index = 1;
                        for (Item itm : arrayItems) {
                            landEcoList[index] = itm.itemName;
                            index++;
                        }

                        renderLandEcoSpinner(landEcoList);

                    }else{
                    Toast.makeText(getApplicationContext(), "List is Empty", Toast.LENGTH_LONG).show();
                    }
                    dialog.dismiss();
                }
            });

        }
    }


    public void renderLandEcoSpinner(String[] mylandEcoList){

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(LandForm.this, android.R.layout.simple_dropdown_item_1line, mylandEcoList) {
            @Override
            public boolean isEnabled(int position) {

                return position != 0;
            }

            @Override
            public View getDropDownView(int position, View convertView,
                                        @NonNull ViewGroup parent) {
                View view = super.getDropDownView(position, convertView, parent);
                TextView tv = (TextView) view;
                if (position == 0) {
                    tv.setTextColor(getResources().getColor(R.color.grey_400));
                } else {
                    tv.setTextColor(getResources().getColor(R.color.green_800));
                }
                tv.setBackgroundColor(getResources().getColor(R.color.green_50));
                return view;
            }
        };
        adapter.setDropDownViewResource(R.layout.spinner_dropdown_item);
        landType.setAdapter(adapter);
        landType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

    }

}


