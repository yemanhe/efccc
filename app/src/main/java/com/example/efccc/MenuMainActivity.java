package com.example.efccc;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.view.View;


public class MenuMainActivity extends AppCompatActivity {

    SharedPreferences myPref;
    SharedPreferences.Editor myeditor;

    CardView land, ecosystem, projectP;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

//        // Todo :
//        // Remove this code section it's added because of the server is unable to verify user
//
//        myPref = getApplicationContext().getSharedPreferences("pref", 0);
//        myeditor = myPref.edit();
//
//        myeditor.putBoolean("Is_Auth", true);
//        myeditor.putString("PhoneNum", "251923002413");
//        myeditor.commit();







        ecosystem = findViewById(R.id.cardView_ecosystem);
        land = findViewById(R.id.cardView_land);
        projectP = findViewById(R.id.cardView_project_p_menu);

        land.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MenuMainActivity.this, MenuLandSector.class);
                intent.putExtra("pageKey", "1");
                startActivity(intent);
            }
        });
        ecosystem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MenuMainActivity.this, MenuEcosystemSector.class);
                intent.putExtra("pageKey", "2");
                startActivity(intent);
            }
        });
        projectP.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MenuMainActivity.this, MenuProjectManagement.class);
                intent.putExtra("pageKey", "3");
                startActivity(intent);
            }
        });

//        SharedPreferences myPref = getApplicationContext().getSharedPreferences("pref" , 0);

//        String pho = myPref.getString("PhoneNum","notfound");
//        boolean auth = myPref.getBoolean("Is_Auth",false);

//        Toast.makeText(getApplicationContext(),pho + " " + "auth " + auth,Toast.LENGTH_LONG).show();


    }


}
