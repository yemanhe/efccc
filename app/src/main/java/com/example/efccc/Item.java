package com.example.efccc;

public class Item {

    public  int ids;
    public  String itemName;

    public Item(int ids, String itemName){
        this.ids = ids;
        this.itemName = itemName;

    }

    public int getIds() {
        return ids;
    }

    public void setIds(int ids) {
        this.ids = ids;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }
}
