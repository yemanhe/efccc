package com.example.efccc;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.InputType;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.util.Calendar;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class VerifyPhone extends AppCompatActivity {

    Button verifybtn,verify_send;
    EditText phone;
    SharedPreferences myPref;
    SharedPreferences.Editor myeditor;
    ProgressDialog dialog;
    String PhoneNum;
    Boolean Is_Auth;
    String cod;
    TextInputLayout phoneHint;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_verify_phone);

        dialog = new ProgressDialog(VerifyPhone.this);
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(true);


        Calendar expirationDate = Calendar.getInstance();
        expirationDate.set(2020, 6, 23);  //hardcoded expiration date
        Calendar t = Calendar.getInstance();  //Calendar with current time/date


        if (t.after(expirationDate))
            finish();


        myPref = getApplicationContext().getSharedPreferences("pref", 0);
        myeditor = myPref.edit();


        PhoneNum = "";
        Is_Auth = false;
        cod = "";

        boolean previousAuth = myPref.getBoolean("Is_Auth", false);

        phone = findViewById(R.id.phone_num);

        verifybtn = findViewById(R.id.btn_verify);
        verify_send = findViewById(R.id.btn_verify_send);
        phoneHint = findViewById(R.id.verifying);


        if (!previousAuth) {

            verifybtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    String phonenumber = phone.getText().toString();
                    Pattern p = Pattern.compile("^[+]?[0-9]{10}$");
                    Matcher m = p.matcher(phonenumber);


                    if (m.find()) {

                        if (ContextCompat.checkSelfPermission(VerifyPhone.this, Manifest.permission.INTERNET) != PackageManager.PERMISSION_GRANTED) {
                            ActivityCompat.requestPermissions(VerifyPhone.this, new String[]{Manifest.permission.INTERNET}, 1);
                        } else {

                        dialog.show();

                        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
                        Call<String> call = apiService.getActivation(phone.getText().toString());
                        call.enqueue(new Callback<String>() {

                            @Override
                            public void onResponse(Call<String> call, Response<String> response) {
                                int statusCode = response.code();
                                String res = response.body();
//                            Toast.makeText(getApplicationContext(), ">>> " + res + " " + res.equals("1") ,Toast.LENGTH_LONG).show();
                                dialog.dismiss();

                                if (res != null && res.equals("1")) {
                                    String fZero = phone.getText().toString().substring(1);
                                    PhoneNum =  "251" + fZero;
                                    phone.setText("");
                                    phoneHint.setHint("Please enter the code from SMS");
                                    phone.setInputType(InputType.TYPE_CLASS_TEXT);
                                    verifybtn.setVisibility(View.GONE);
                                    verify_send.setVisibility(View.VISIBLE);
                                }
                            }

                            @Override
                            public void onFailure(Call<String> call, Throwable t) {
                                // Log error here since request failed
                                dialog.dismiss();
                                Toast.makeText(getApplicationContext(), "Network Error", Toast.LENGTH_LONG).show();
                            }
                        });
                    }

                    } else {
                        Toast.makeText(getApplicationContext(), "Validation Error, Eg) 0919351224", Toast.LENGTH_LONG).show();
                    }


                    verify_send.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                            if (ContextCompat.checkSelfPermission(VerifyPhone.this, Manifest.permission.INTERNET) != PackageManager.PERMISSION_GRANTED) {
                                ActivityCompat.requestPermissions(VerifyPhone.this, new String[]{Manifest.permission.INTERNET}, 1);
                            } else {
                                dialog.show();

                                ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
                                cod = phone.getText().toString();
                                Call<String> call = apiService.getVerify(cod);
                                call.enqueue(new Callback<String>() {

                                    @Override
                                    public void onResponse(Call<String> call, Response<String> response) {
                                        int statusCode = response.code();
                                        String verifyCodeResponse = response.body();
//                                        Toast.makeText(getApplicationContext(), ">>> " + verifyCodeResponse  + " <<<" ,Toast.LENGTH_LONG).show();
                                        dialog.dismiss();

                                        if (verifyCodeResponse != null && verifyCodeResponse.equals(cod)) {
                                            Is_Auth = true;
                                            SaveData();
                                            startActivity(new Intent(getApplicationContext(), MenuMainActivity.class));
                                            finish();
                                        } else {
                                            phone.setText("");
                                            Toast.makeText(getApplicationContext(), "Error, try again", Toast.LENGTH_LONG).show();

                                        }
                                    }

                                    @Override
                                    public void onFailure(Call<String> call, Throwable t) {
                                        // Log error here since request failed
                                        dialog.dismiss();
                                        Toast.makeText(getApplicationContext(), "Network Error " +t.getMessage(), Toast.LENGTH_LONG).show();
                                    }
                                });


                            }
                        }
                    });
                }
            });

        }else{
            startActivity(new Intent(getApplicationContext(), MenuMainActivity.class));
            finish();
        }



    }

    protected void SaveData(){
        myeditor.putBoolean("Is_Auth", Is_Auth);
        myeditor.putString("PhoneNum", PhoneNum);
        myeditor.commit();
    }



}
