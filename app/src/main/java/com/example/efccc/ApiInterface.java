package com.example.efccc;

import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Response;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;
import retrofit2.http.QueryMap;

public interface ApiInterface {


    @GET("Kebele/GetbyPhone")
    Call<List<Item>> getKebele(@Query("phoneNo") String phone);


    @GET("LandCoverType")
    Call<List<Item>> getLandCoverType();


    @GET("EcosystemType")
    Call<List<Item>> getEcoSystemType();


    @GET("OutComes")
    Call<List<Item>> getOutComes();


    @GET("OutPuts")
    Call<List<Item>> getOutPuts();


    @GET("Activations/GetActivationCodebyPhone")
    Call<String> getActivation(@Query("phoneNo") String phone);


    @GET("Activations/setActivationMyPhone")
    Call<String> getVerify(@Query("activationCode") String activation);


    @GET("Data/SetData")
    Call<String> sendData(
            @Query("inputData") String inputData,
            @Query("phoneNo") String phoneNo
    );

}
